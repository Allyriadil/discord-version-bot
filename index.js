const Discord = require("discord.js");
const config = require("./config.json");
const fetch = require("node-fetch");

const client = new Discord.Client();

// Declare variables for version comparison
let ver1;
let ver2;

client.login(config.BOT_TOKEN);

client.on("ready", () => {
  console.log("Bot is up and running :)");
  // Initialize version loop
  var timer = setInterval(versionLoop, 5000);
  function versionLoop() {
    fetch("https://rc.nl.carsys.online/version.json")
      .then((response) => response.json())
      .then((data) => {
        if (typeof ver1 === "undefined" && typeof ver2 === "undefined") {
          ver1 = data;
          ver2 = data;
        } else if (ver1["Commit id"] !== ver2["Commit id"]) {
          ver1 = data;
          ver2 = data;
          client.channels.cache
            .get("799606363940061216")
            .send(
              "RC version update! @everyone \n" +
                `Version: ${ver2["Version:"]}\n` +
                `Build date: ${ver2["Build date"]}\n` +
                `Build number: ${ver2["Build number"]}\n` +
                `Commit id: ${ver2["Commit id"]}`
            );
        } else {
          ver2 = data;
        }
      });
  }
});
